import 'dart:ui';

import 'package:flutter/material.dart';
import 'maina.dart';
import 'loginPage.dart';

void main() {
  runApp(MaterialApp(
    title: 'Parking Reservation',
    debugShowCheckedModeBanner: false,
    home: FirstPage(),
    theme: ThemeData(primarySwatch: Colors.green),
  ));
}

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Container(
              child: Text(
                'Parking Car',
                style: TextStyle(
                  fontSize: 50,
                  fontWeight: FontWeight.w900,
                  letterSpacing: 1,
                ),
              ),
            ),
            mainImage,
            Container(
              width: 440,
              child: ElevatedButton(
                child: Text('เข้าสู่ระบบ'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

final mainImage = Image.asset(
  'images/pak.jpg',
  fit: BoxFit.cover,
  width: 500,
);
